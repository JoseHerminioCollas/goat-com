import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Dialog from './dialog';

class App extends Component {
  render() {
    return (
        <MuiThemeProvider>
          <Dialog />
        </MuiThemeProvider>
    );
  }
}

export default App;
