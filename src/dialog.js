import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import FontIcon from 'material-ui/FontIcon';
import {greenA200} from 'material-ui/styles/colors';
import axios from 'axios';

const iconStyles = {
  color: '#777',
  fontSize: '32px',
  margin: '12px',
  marginRight: 24,
  cursor: 'pointer'
};

export default class DialogExampleSimple extends React.Component {
  state = {
    open: false,
    vals: {name: '', subject: '', email: '', message: ''}
  };
  handleOpen = () => {
    this.setState({open: true});
  };
  handleClose = (e, a, b) => {
    axios.post('http://goatstone.net:8080/msg', {
      name: this.state.vals.name,
      email: this.state.vals.email,
      subject: this.state.vals.subject,
      message: this.state.vals.message
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });   

    this.setState({open: false});
  };
  updateMessage = (event, inputValue) => {
      let key = event.target.id
      if(this.state.vals[key] === undefined) return
      let newState = Object.assign({}, this.state.vals, { [key]: inputValue})      
      this.setState({vals: newState})
  }
  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />,
      <FlatButton
        label="Send"
        primary={true}
        keyboardFocused={true}
        onClick={this.handleClose}
      />,
    ];
    return (
      <div>
        <FontIcon 
        className="material-icons" 
        style={iconStyles}
        onClick={this.handleOpen}
        hoverColor={greenA200}>
          mail_outline</FontIcon>
        <Dialog
          title="Send email to Goatstone"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
        <TextField
        hintText="Name"
        id="name"
        onChange={this.updateMessage}
        /><br />
        <TextField
        hintText="Subject"
        id="subject"
        onChange={this.updateMessage}
        /><br />
        <TextField
        hintText="Email address"
        id="email"
        onChange={this.updateMessage}
        /><br />
        <TextField
        hintText="Message"
        id="message"
        multiLine={true}
        rows={2}
        rowsMax={4}
        onChange={this.updateMessage}
        /><br />        
        </Dialog>
      </div>
    );
  }
}